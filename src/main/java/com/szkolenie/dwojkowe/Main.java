package com.szkolenie.dwojkowe;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Integer> bits = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");

        int number = sc.nextInt();

        while (number > 0) {
            bits.add(0, number % 2);
            number /= 2;
        }

        for (int el : bits) {
            System.out.print(el);
        }
    }
}
